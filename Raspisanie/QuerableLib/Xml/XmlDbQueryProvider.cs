﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Serialization;
using QuerableLib.Infrastructure;

namespace QuerableLib.Xml
{
    public class XmlDbQueryProvider : QueryProvider
    {
        private readonly string _filename;
        private readonly Func<XmlAttributeOverrides> _createOverrides;

        public XmlDbQueryProvider(string filename, Func<XmlAttributeOverrides> createOverrides)
        {
            _filename = filename;
            _createOverrides = createOverrides;
        }

        public override string GetQueryText(Expression expression)
        {
            return "GetAll";
        }

        public override object Execute(Expression expression)
        {
            var list = Process(expression);

            return list;
        }
        private object Process(Expression expression)
        {
            switch (expression.NodeType)
            {
                case ExpressionType.Call:
                    MethodCallExpression call = (MethodCallExpression)expression;
                    var res1 = Process(call.Arguments.First());
                    var args = new ArrayList(){res1};
                    foreach (var callArgument in call.Arguments.Skip(1))
                    {
                        switch (callArgument)
                        {
                            case UnaryExpression u:
                                args.Add(u.Operand);
                                break;
                            case ConstantExpression c:
                                args.Add(c.Value);
                                break;
                            default:
                                args.Add(callArgument);
                                break;
                        }
                    }
                    var res = call.Method.Invoke(null, args.ToArray());
                    return res;
                case ExpressionType.Constant when expression.ToString() == "GetAll":
                    var list = GetList(expression);
                    return Queryable.AsQueryable((IEnumerable)list);
                default: throw new ArgumentException("Unsupported node type");
            }
        }
        private object GetList(Expression expression)
        {
            if (expression.ToString() != "GetAll")
                throw new ArgumentException("Not GetAll");
            Type elementType = TypeSystem.GetElementType(expression.Type);
            var listType = typeof(System.Collections.Generic.List<>).MakeGenericType(elementType);
            object list;

            XmlSerializer formatter = new XmlSerializer(listType, _createOverrides());

            try
            {
                using (FileStream fs = new FileStream(_filename, FileMode.Open))
                {
                    list = formatter.Deserialize(fs);
                }
            }
            catch (System.IO.FileNotFoundException e)
            {
                list = Activator.CreateInstance(listType);
            }

            return list;
        }
    }
}
