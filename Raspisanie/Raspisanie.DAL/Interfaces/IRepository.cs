﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Raspisanie.DAL.Models;

namespace Raspisanie.Interfaces
{
    public interface IRepository<T> where T : BaseModel
    {
        void Create(T obj);
        T Read(int id);
        void Update(T obj);
        void Delete(int id);
        IEnumerable<T> GetAll();
    }
}
