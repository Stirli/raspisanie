﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Raspisanie.DAL.Infrastructure;
using Raspisanie.DAL.Models;
using Raspisanie.Interfaces;

namespace Raspisanie.DAL.XMLRepositories
{
    public class XmlLessonsRepository : XmlBaseRepository<Lesson>
    {
        public XmlLessonsRepository(string filename, XmlDbContext xmlDbContext) : base(filename)
        {
        }
       
        protected override XmlAttributeOverrides CreateOverrides()
        {
            var overrides = new XmlAttributeOverrides();
            overrides.Ignore<Lesson>(teacher => teacher.Subject, teacher => teacher.Teacher);
            return overrides;
        }
    }
}