﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using QuerableLib;
using QuerableLib.Xml;
using Raspisanie.DAL.Infrastructure;
using Raspisanie.DAL.Models;
using Raspisanie.Interfaces;

namespace Raspisanie.DAL.XMLRepositories
{
    public abstract class XmlBaseRepository<T> : IRepository<T> where T : BaseModel
    {
        protected readonly string _filename;
        protected List<T> _list;
        public XmlBaseRepository(string filename)
        {
            _filename = filename;
            _list = new List<T>();
        }


        public virtual void Create(T obj)
        {
            obj.Id = this.GetLastId();
            _list.Add(obj);
            SaveContext();
        }

        public virtual T Read(int id)
        {
            return GetAll().FirstOrDefault(model => model.Id == id);
        }

        public virtual void Update(T obj)
        {
            GetAll();
            var find = _list.Find(i => i.Id == obj.Id);
            _list.Remove(find);
            _list.Add(obj);
            SaveContext();
        }

        public virtual void Delete(int id)
        {
            GetAll();
            var find = _list.Find(i => i.Id == id);
            _list.Remove(find);
            SaveContext();
        }

        public virtual IEnumerable<T> GetAll()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<T>), CreateOverrides());
            try
            {
                using (FileStream fs = new FileStream(_filename, FileMode.Open))
                {
                    _list = (List<T>)formatter.Deserialize(fs);
                }
            }
            catch (System.IO.FileNotFoundException e)
            {
                return _list = new List<T>();
            }

            return _list;
        }
        public virtual Query<T> GetAll2()
        {
            QueryProvider p = new XmlDbQueryProvider(_filename, CreateOverrides);
            return new Query<T>(p);
        }


        protected virtual void SaveContext()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<T>), CreateOverrides());
            using (FileStream fs = new FileStream(_filename, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, _list);
            }
        }

        protected abstract XmlAttributeOverrides CreateOverrides();
    }
}