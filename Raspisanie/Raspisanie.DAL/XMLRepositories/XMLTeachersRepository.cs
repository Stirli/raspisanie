﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Serialization;
using Raspisanie.DAL.Infrastructure;
using Raspisanie.DAL.Models;

namespace Raspisanie.DAL.XMLRepositories
{
    public class XmlTeachersRepository : XmlBaseRepository<Teacher>
    {
        private readonly XmlDbContext _xmlDbContext;

        public XmlTeachersRepository(string filename, XmlDbContext xmlDbContext) : base(filename)
        {
            _xmlDbContext = xmlDbContext;
        }


        protected override XmlAttributeOverrides CreateOverrides()
        {
            var overrides = new XmlAttributeOverrides();
            overrides.Ignore<Teacher>(teacher => teacher.Lessons, teacher => teacher.Subjects);
            return overrides;
        }
    }
}
