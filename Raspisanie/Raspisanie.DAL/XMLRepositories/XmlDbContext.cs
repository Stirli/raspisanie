﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Raspisanie.DAL.Models;
using Raspisanie.Interfaces;

namespace Raspisanie.DAL.XMLRepositories
{
    public class XmlDbContext
    {
        const string teachersXml = "teachers.xml";
        const string lessonsXml = "lessons.xml";
        const string subjectsXml = "subjects.xml";
        public XmlDbContext()
        {
            Teachers = new XmlTeachersRepository(teachersXml, this);
            Lessons = new XmlLessonsRepository(lessonsXml, this);
            Subjects = new XmlSubjectsRepository(subjectsXml, this);
        }
        public IRepository<Teacher> Teachers { get; set; }
        public IRepository<Lesson> Lessons { get; set; }
        public IRepository<Subject> Subjects { get; set; }

        public IRepository<BaseModel> GetRepository(string name)
        {
            return (IRepository<BaseModel>)GetType().GetProperty(name).GetValue(this);
        }

        public void DropDatabase()
        {
            File.Delete(teachersXml);
            File.Delete(lessonsXml);
            File.Delete(subjectsXml);
        }
    }
}
