﻿using System.Xml.Serialization;
using Raspisanie.DAL.Infrastructure;
using Raspisanie.DAL.Models;

namespace Raspisanie.DAL.XMLRepositories
{
    public class XmlSubjectsRepository : XmlBaseRepository<Subject>
    {
        public XmlSubjectsRepository(string filename, XmlDbContext xmlDbContext) : base(filename)
        {
        }

        protected override XmlAttributeOverrides CreateOverrides()
        {
            var overrides = new XmlAttributeOverrides();
            overrides.Ignore<Subject>(s => s.Teachers);
            return overrides;
        }
    }
}