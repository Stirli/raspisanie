﻿using System;
using System.Linq;
using System.Xml.Linq;
using Raspisanie.DAL.Models;
using Raspisanie.Interfaces;

namespace Raspisanie.DAL.Infrastructure
{
    public static class RepoHelper
    {
        public static int GetLastId<T>(this IRepository<T> repo) where T : BaseModel
        {
            int max = 0;
            foreach (var t in repo.GetAll()) max = Math.Max(max, t.Id);

            return max;
        }

    }
}