﻿namespace Raspisanie.DAL.Models
{
    public class Lesson:BaseModel
    {
        public int Parallel { get; set; }
        public char Letter { get; set; }
        public string ClassRoom { get; set; }
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
    }
}
