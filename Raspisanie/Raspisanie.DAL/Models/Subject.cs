﻿using System.Collections.Generic;

namespace Raspisanie.DAL.Models
{
    public class Subject:BaseModel
    {
        public string Name { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}