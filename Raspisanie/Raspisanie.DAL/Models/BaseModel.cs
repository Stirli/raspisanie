﻿using System.Linq;

namespace Raspisanie.DAL.Models
{
    public class BaseModel
    {
        public int Id { get; set; }
        public BaseModel()
        {
            foreach (var propertyInfo in GetType().GetProperties().Where(pi=>pi.PropertyType==typeof(string)))
            {
                propertyInfo.SetValue(this,"");
            }
        }
    }
}
