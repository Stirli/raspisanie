﻿using System.Collections.Generic;

namespace Raspisanie.DAL.Models
{
    public class Teacher : BaseModel
    {
        public Teacher()
        {
            Subjects = new List<Subject>();
            Lessons = new List<Lesson>();
        }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int DesiredHours { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
        public virtual ICollection<Lesson> Lessons { get; set; }
    }
}
