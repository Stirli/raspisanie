﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Raspisanie.DAL.Models;
using Raspisanie.DAL.XMLRepositories;
using Raspisanie.Interfaces;
using Xunit;

namespace Raspisanie.DAL.XUnitTests
{
    [Collection("Our Test Collection #1")]
    public class XmlLessonsRepositoryTests : IDisposable
    {
        private XmlDbContext xmlDbContext;
        public XmlLessonsRepositoryTests()
        {
            xmlDbContext = new XmlDbContext();
            xmlDbContext.DropDatabase();
        }

        [Fact]
        public void CreateTest()
        {
            IRepository<Lesson> repo = new XmlDbContext().Lessons;
            var teacher = new Lesson()
            {
                Id = 1,
                Parallel = 5,
                Letter = 'a',
                ClassRoom = "23",
                SubjectId = 1,
                TeacherId = 1
            };

            repo.Create(teacher);
            var list = repo.GetAll().ToList();
            Assert.Collection(list, t =>
            {
                Assert.Equal(teacher.Id,t.Id);
                Assert.Equal(teacher.Parallel, t.Parallel);
                Assert.Equal(teacher.Letter, t.Letter);
                Assert.Equal(teacher.ClassRoom, t.ClassRoom);
                Assert.Equal(teacher.SubjectId, t.SubjectId);
                Assert.Equal(teacher.TeacherId, t.TeacherId);
            });
        }

        public void Dispose()
        {
            xmlDbContext.DropDatabase();
        }
    }
}