﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Raspisanie.DAL.Models;
using Raspisanie.DAL.XMLRepositories;
using Raspisanie.Interfaces;
using Xunit;

namespace Raspisanie.DAL.XUnitTests
{
    [Collection("Our Test Collection #1")]
    public class XmlTeachersRepositoryTests:IDisposable
    {
        private XmlDbContext xmlDbContext;

        public XmlTeachersRepositoryTests()
        {
            xmlDbContext = new XmlDbContext();
            xmlDbContext.DropDatabase();
        }

        [Fact]
        public void CreateTest()
        {
            IRepository<Teacher> repo = new XmlDbContext().Teachers;
            var teacher = new Teacher()
            {
                Id = 1,
                DesiredHours = 5,
                FirstName = "1",
                MiddleName = "2",
                LastName = "3",
                Lessons = new List<Lesson>() { new Lesson() { Letter = 'a' } },
                Subjects = new List<Subject>() { new Subject() {Name = "asd"} }
            };

            repo.Create(teacher);
            var list = repo.GetAll().ToList();
            Assert.Collection(list, t =>
            {
                Assert.Equal(teacher.Id,t.Id);
                Assert.Equal(teacher.DesiredHours,t.DesiredHours);
                Assert.Equal(teacher.FirstName,t.FirstName);
                Assert.Equal(teacher.LastName,t.LastName);
                Assert.Equal(teacher.MiddleName,t.MiddleName);
                Assert.Equal(0,t.Lessons.Count);
                Assert.Equal(0,t.Subjects.Count);
            });
            }

        public void Dispose()
        {
            xmlDbContext.DropDatabase();
        }
    }
}
