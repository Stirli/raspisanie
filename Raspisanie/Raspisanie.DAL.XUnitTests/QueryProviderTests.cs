﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raspisanie.DAL.Models;
using Raspisanie.DAL.XMLRepositories;
using Raspisanie.Interfaces;
using Xunit;

namespace Raspisanie.DAL.XUnitTests
{
    [Collection("Our Test Collection #1")]
    public class QueryProviderTests : IDisposable
    {
        private XmlSubjectsRepository repo;
        private Subject teacher;
        private XmlDbContext xmlDbContext;
        public QueryProviderTests()
        {
            xmlDbContext = new XmlDbContext();
            xmlDbContext.DropDatabase();
            repo = (XmlSubjectsRepository)xmlDbContext.Subjects;
            teacher = new Subject()
            {
                Id = 1,
                Name = "География"
            };

            repo.Create(teacher);
        }

        public void Dispose()
        {
            xmlDbContext.DropDatabase();
        }
        [Fact]
        public void TestQuery()
        {
            var list = repo.GetAll2()
                 .Where(subject => subject.Id == teacher.Id)
                 .Select(subject => subject.Id.ToString() + subject.Name)
                 .Union(new[] { "asdasd" })
                 .ToList();

            Assert.Collection(list, t =>
            {
                Assert.Equal(teacher.Id.ToString() + teacher.Name, t);
            },
                t =>
            {

                Assert.Equal("asdasd", t);
            });
        }
        [Fact]
        public void TestQuery2()
        {
            var t = repo.GetAll2()
                 .Where(subject => subject.Id == teacher.Id)
                 .Select(subject => subject.Id.ToString() + subject.Name)
                 .Concat(new[] { "asdasd" })
                 .First();
            Assert.Equal(teacher.Id.ToString() + teacher.Name, t);
        }
    }
}
