﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Raspisanie.DAL.Models;
using Raspisanie.DAL.XMLRepositories;
using Raspisanie.Interfaces;
using Xunit;

namespace Raspisanie.DAL.XUnitTests
{
    [Collection("Our Test Collection #1")]
    public class XmlSubjectsRepositoryTests : IDisposable
    {
        private XmlDbContext xmlDbContext;

        public XmlSubjectsRepositoryTests()
        {
            xmlDbContext = new XmlDbContext();
            xmlDbContext.DropDatabase();
        }

        [Fact]
        public void CreateTest()
        {
            IRepository<Subject> repo = new XmlDbContext().Subjects;
            var teacher = new Subject()
            {
                Id = 1,
                Name = "География"
            };

            repo.Create(teacher);
            var list = repo.GetAll().ToList();
            Assert.Collection(list, t =>
            {
                Assert.Equal(teacher.Id, t.Id);
                Assert.Equal(teacher.Name, t.Name);
            });
        }

        public void Dispose()
        {
            xmlDbContext.DropDatabase();
        }
    }
}
